require 'colorize'

require_relative 'piece'
require_relative 'board'
require_relative 'player'

class Checkers
  attr_reader :board, :players

  def initialize
    @board = Board.new
    @players = @board.colors.map { |color| HumanPlayer.new(color, @board) }
  end

  def play
    game_over = false
    until game_over
      self.players.each do |player|

        self.board.display
        puts "#{player.color.to_s.capitalize}'s turn"
        player.play_turn

        # Check for win
        if winner?(player.color)
          puts "#{player.color.to_s.capitalize} wins!"
          game_over = true
        end
      end
    end

    self.board.display
    #Game Over!
  end

  def winner?(color)
    colors = self.board.colors
    opponent = { colors[0] => colors[1],
                 colors[1] => colors[0] }

    # You win if your opponent has no pieces on the board
    self.board.pieces(opponent[color]).empty?
  end
end



if $PROGRAM_NAME == __FILE__
  game = Checkers.new
  game.play
end
