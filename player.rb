class InputError < RuntimeError
end

class HumanPlayer
  attr_accessor :color, :board

  def initialize(color, board)
    @color = color
    @board = board
  end

  def play_turn
    begin
      positions = get_move_sequence
      start = positions[0]
      move_seq = positions[1..-1]

      self.board[start].perform_moves(move_seq)

    rescue InvalidMoveError => e
      puts e.message
      retry
    end
  end

  # Returns array containing start position and moves you want to make
  def get_move_sequence
    begin
      puts "Enter move sequence as space-separated positions: "
      input = gets.chomp

      # Make sure each position is formatted correctly
      input.split(' ').each do |pos_str|
        unless (pos_str.length == 2 && pos_str[0].between?('a','h') &&
                                       pos_str[1].between?('1','8'))
          raise InputError.new("Invalid position format")
        end
      end

      positions = input.split(' ').map { |pos_str| input_to_pos(pos_str) }

      start = positions[0]
      raise InputError.new("No piece there") if self.board.empty_at?(start)
      raise InputError.new("Not your piece to move") unless self.board.color_at(start) == self.color

    rescue InputError => e
      puts e.message
      retry
    end

    positions
  end

  private

  def input_to_pos(input)
    col = input[0].ord - 'a'.ord
    row = 8 - input[1].to_i
    [row, col]
  end
end