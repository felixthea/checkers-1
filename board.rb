# encoding: UTF-8

class Board
  attr_accessor :rows, :colors

  def initialize(colors = [:black, :red], fill_board = true)
    @rows = Array.new(8) { Array.new(8) }
    @colors = colors
    set_pieces if fill_board
  end

  def pieces(color)
    self.rows.flatten.select { |sqr| !sqr.nil? && sqr.color == color }
  end

  def dup
    dup_board = self.class.new(self.colors, false)

    self.rows.each_with_index do |row, row_idx|
      row.each_with_index do |sqr, col_idx|
        dup_sqr = sqr.nil? ? nil : sqr.dup(dup_board)
        dup_board[[row_idx, col_idx]] = dup_sqr
      end
    end

    dup_board
  end

  def display
    background_colors = [:white, :light_white]
    border_colors = { color: :white, background: :black }
    column_markers = ("    "+("a".."h").to_a.join("  ")+"    ").colorize(border_colors)

    puts column_markers
    self.rows.each_with_index do |row, row_idx|
      row_marker = " #{8-row_idx} ".colorize(border_colors)

      print row_marker
      row.each_with_index do |sqr, col_idx|
        char = sqr.nil? ? " " : sqr.render.colorize(sqr.color)
        print " #{char} ".colorize(background: background_colors[(row_idx+col_idx)%2])
      end
      puts row_marker
    end
    puts column_markers
  end

  def [](pos)
    raise ArgumentError.new("Invalid position #{pos}") unless self.valid_pos?(pos)
    self.rows[pos[0]][pos[1]]
  end

  def []=(pos, value)
    raise ArgumentError.new("Invalid position #{pos}") unless self.valid_pos?(pos)
    self.rows[pos[0]][pos[1]] = value
  end

  def valid_pos?(pos)
    pos[0].between?(0,7) && pos[1].between?(0,7)
  end

  def empty_at?(pos)
    self[pos].nil?
  end

  def color_at(pos)
    return nil if self[pos].nil?
    self[pos].color
  end

  private

  def set_pieces
    self.colors.each do |color|
      start_rows = (color == self.colors[0] ? (0..2) : (5..7)).to_a

      start_rows.each do |row|
        self.rows[row].each_index do |col|
          if (row + col) % 2 == 1
            pos = [row, col]
            self[pos] = Piece.new(pos, color, self)
          end
        end
      end
    end
  end
end
