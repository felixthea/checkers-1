# encoding: UTF-8

class InvalidMoveError < RuntimeError
end

class Piece
  attr_accessor :position, :color, :board, :promoted

  def initialize(position, color, board, promoted = false)
    @position, @color, @board, @promoted = position, color, board, promoted
  end

  def dup(board)
    self.class.new(self.position, self.color, board, self.promoted)
  end

  def render
    promoted ? "★" : "⚉"
  end

  def slide_moves
    pos = self.position
    moves = []

    # For all possible moves
    deltas.each do |delta|
      move = [pos[0] + delta[0], pos[1] + delta[1]]
      next unless self.board.valid_pos?(move)

      # Include it if it ends on an empty square
      moves << move if self.board.empty_at?(move)
    end

    moves
  end

  def jump_moves
    pos = self.position
    moves = []

    # For each adjacent diagonal square
    deltas.each do |delta|
      adjacent = [pos[0] + delta[0], pos[1] + delta[1]]
      next unless self.board.valid_pos?(adjacent)

      # If it contains an enemy player
      if self.board.color_at(adjacent) == enemy

        # Include the jump move if the square behind it is empty
        jump_move = [adjacent[0] + delta[0], adjacent[1] + delta[1]]
        next unless self.board.valid_pos?(jump_move)

        moves << jump_move if self.board.empty_at?(jump_move)
      end
    end

    moves
  end

  def perform_slide(pos)
    unless self.slide_moves.include?(pos)
      raise InvalidMoveError.new("Cannot slide from #{self.position} to #{pos}")
    end

    move(pos)
  end

  def perform_jump(pos)
    unless self.jump_moves.include?(pos)
      raise InvalidMoveError.new("Cannot jump from #{self.position} to #{pos}")
    end

    cur_pos = self.position
    jumped_pos = [(cur_pos[0]+pos[0])/2, (cur_pos[1]+pos[1])/2]

    move(pos)
    self.board[jumped_pos] = nil # Remove captured piece
  end

  def valid_move_seq?(move_seq)
    dup_board = self.board.dup
    dup_piece = dup_board[self.position]

    begin
      dup_piece.perform_moves!(move_seq)
    rescue
      return false
    end
    true
  end

  def perform_moves(move_seq)
    if valid_move_seq?(move_seq)
      self.perform_moves!(move_seq)
    else
      raise InvalidMoveError.new("Invalid move sequence")
    end
  end

  def perform_moves!(move_seq)
    raise InvalidMoveError.new("Must make a move") if move_seq.empty?

    pos = self.position
    if move_seq.length == 1 && move_type(pos, move_seq[0]) == :slide
      # If the only specified move is a slide
      perform_slide(move_seq[0])
    else
      # Otherwise everything should be a jump
      move_seq.each do |move|
        unless move_type(pos, move) == :jump
          raise InvalidMoveError.new("Invalid move sequence")
        end

        perform_jump(move)
        pos = move
      end
    end
  end

  private

  def deltas
    dr = (self.color == self.board.colors[0]) ? 1 : -1 # Change in row
    deltas = [[dr, 1], [dr,-1]] # Slide forward diagonally
    deltas += [[-dr, 1], [-dr,-1]] if self.promoted # Slide backwards diagonally
    deltas
  end

  def enemy
    # Find enemy color
    if self.color == self.board.colors[0]
      self.board.colors[1]
    else
      self.board.colors[0]
    end
  end

  def move(pos)
    self.board[pos] = self # Add piece to final position on board
    self.board[self.position] = nil # Remove piece from old position
    self.position = pos # Update piece's position
  end

  def move_type(start, finish)
    dr = finish[0] - start[0]
    dc = finish[1] - start[1]

    if dr.abs == 1 && dc.abs == 1
      :slide
    elsif dr.abs == 2 && dc.abs == 2
      :jump
    else
      nil
    end
  end
end